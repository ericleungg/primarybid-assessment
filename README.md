# PrimaryBid Assessment


## Description

This is a full-stack application that provides URL shortening functionality. The application is built with **Vue.js** for the frontend, with **Vanilla CSS** styling, and **Express.js** for the backend. URL data are stored in **MongoDB**. Codes are written in **Typescript**. Tests are conducted under the **Jest** framework. The system will be run using **Docker Compose**.


#### Pre-requisite

- Docker (with docker-compose)
- npm
- Node.js
- MongoDB


#### Framework

- Vue.js
- Express.js
- Jest


## Features

1. Add URL with shortening function to 8 characters (lowercase-alphanumeric)
2. Each shortened URL are unique
3. List of shortened URLs is displayed
4. Review the original URL by clicking on the shortened URL
5. The URL are not redirectable
6. List of URLs can be removed


## Deployment

### 1. Clone the project

```bash
git clone https://gitlab.com/ericleungg/primarybid-assessment.git
cd primarybid-assessment
```

### 2a. Start the application

```bash
docker-compose up
```

> The application has been tested running in **macOS**

> If running on **Windows** and getting into problem with the backend or frontend services, please try the following:

> Save `/frontend/docker/entrypoint.sh` and `/backend/docker/entrypoint.sh` in Unix file format. Retry `$ docker-compose up --build`


### 2b. Access to the application

- It will take quite a long while to download the required modules until the applications start running
- Once they have all started, open a browser and go to [http://localhost:8081](url)
- You should see a page with title "PrimaryBid Assessment"

### 3. Testing frontend and backend

```bash
docker-compose run --rm frontend npm test
docker-compose run --rm backend npm test
```









