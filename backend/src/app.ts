import {createServer} from './index'
import logger from './log'

createServer()
	.catch(err => {
		logger.error(`Error: ${err}`)
	})