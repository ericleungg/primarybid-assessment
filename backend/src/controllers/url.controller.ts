import { db } from "../models";
import { NextFunction, Request, Response } from 'express';
import logger from '../log';
import Url from '../models/url.model';
import crypto from "crypto";

// Create and Save a new Url
const create = async (req: Request, res: Response, next: NextFunction) => {
    logger.info(req.body);

    // Validate request
	if (!req.body.url) {
	    res.status(400).send({ message: "Content can not be empty!" });
	    return;
	}

    logger.info(req.body.url);

    let shortenedSubDir = "";
    let shortenedUrl = "";
    const longUrl = "https://pbid.io/" + req.body.url
    let checkUrl = {};

    while (true) {
        // Create shortened subdirectory
        shortenedSubDir = crypto.randomBytes(20).toString('hex').substr(0, 8);
        shortenedUrl = "https://pbid.io/" + shortenedSubDir

        // Check if duplicated from DB
        checkUrl = await Url.findOne({url: shortenedUrl})
        if (checkUrl !== null) {
            continue
        }

        // Create a Url
        const url = new Url({
            shortUrl: shortenedUrl,
            longUrl
        });

        return url
            .save()
            .then((result: any) => {
                return res.status(201).json({
                    url: result
                });
            })
            .catch((error: any) => {
                return res.status(500).json({
                    message: error.message,
                    error
                });
            });
    }
};

// Create and Save a new Url
const findAll = (req: Request, res: Response, next: NextFunction) => {
    return Url
        .find()
        .exec()
        .then((urls: any) => {
            return res.status(200).json({
                urls,
                count: urls.length
            });
        })
        .catch((error: any) => {
            return res.status(500).json({
                message: error.message,
                error
            });
        });
};

// Create and Save a new Url
const deleteAll = (req: Request, res: Response, next: NextFunction) => {
    return Url
        .deleteMany()
        .exec()
        .then((result: any) => {
            return res.status(200).json({
                result
            });
        })
        .catch((error: any) => {
            return res.status(500).json({
                message: error.message,
                error
            });
        });
};

export default { create, findAll, deleteAll };
