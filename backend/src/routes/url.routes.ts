import express from 'express';
import { NextFunction, Request, Response } from 'express';
import controller from '../controllers/url.controller';

const router = express.Router();

router.get('/urls', controller.findAll);
router.post('/create/url', controller.create);
router.delete('/delete', controller.deleteAll);

export = router;


