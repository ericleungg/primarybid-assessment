import "../lib/env"

const {
  DB_USER,
  DB_PASSWORD,
  DB_HOST,
  DB_PORT,
  DB_NAME,
} = process.env;

const dbConfig = {
  dbUrl: (process.env.WAIT_HOSTS)
  			? "mongodb://" + process.env.WAIT_HOSTS + `/${DB_NAME}`
  			: `mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`
};

export {dbConfig}