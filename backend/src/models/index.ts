import { dbConfig } from "../config/db.config";
import mongoose from "mongoose";

mongoose.Promise = global.Promise;

const db = {
	mongoose,
	dbUrl: dbConfig.dbUrl,
	urls: require("./url.model")
};

export { db };