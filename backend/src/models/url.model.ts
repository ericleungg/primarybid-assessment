import mongoose, { Schema } from 'mongoose';
import logging from 'winston';

const UrlSchema: Schema = new Schema(
    {
        shortUrl: { type: String, required: true },
        longUrl: { type: String, required: true },
    },
    {
        timestamps: true
    }
);

export default mongoose.model('Url', UrlSchema);