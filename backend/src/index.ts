import http from 'http';
import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import logConfig from "./log";
import logger from './log'
import { db } from "./models";
import urlRoutes from './routes/url.routes';
import {Express} from 'express-serve-static-core'
/* eslint-disable import/first */
import dotenv from 'dotenv'
dotenv.config()


export async function createServer(): Promise<Express> {
  const router = express();

  const corsOptions = {
    origin: "http://localhost:8081"
  };

  router.use(cors(corsOptions));

  // Connect to MongoDB
  await db.mongoose
    .connect(db.dbUrl, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    })
    .then(() => {
      logger.info("Connected to the database!");
    })
    .catch((err: any) => {
      logger.info("Cannot connect to the database!", err);
      process.exit();
    });

  /** Parse the body of the request */
  router.use(bodyParser.urlencoded({ extended: true }));
  router.use(bodyParser.json());

  /** Routes go here */
  router.use('/api', urlRoutes);

  /** Error handling */
  router.use((req, res, next) => {
      const error = new Error('Not found');

      res.status(404).json({
          message: error.message
      });
  });

  const httpServer = http.createServer(router);
  const PORT = process.env.PORT || 8080;
  httpServer.listen(PORT, () => logger.info(`Server is running on port ${PORT}.`));

  return router
}


