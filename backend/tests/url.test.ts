import request from 'supertest';
import {Express} from 'express-serve-static-core'
import {createServer} from '../src/index'

let server: Express

beforeAll(async () => {
  server = await createServer()
})

describe('POST /create/url', () => {
  it('should return 201 & valid URL if url in data payload is not empty', async () => {
    request(server)
      .post(`/api/create/url`)
			.set('Content-type', 'application/json')
      .send({url: "aaaaaaaaaaaaa"})
      .expect('Content-Type', /json/)
      .expect(201)
      .expect((res) => {
      	expect(String(res.body.url.url)).toMatch(/https:\/\/pbid\.io\/[a-z0-9]{8}/)
    	})
  })

  it('should return 400 if no data payload is sent', async () => {
    request(server)
      .post(`/api/create/url`)
      .expect(400)
      .end((err, res) => {
        if (err) return;
      })
  })

  it('should return 400 if url is empty in data payload', async () => {
    request(server)
      .post(`/api/create/url`)
			.set('Content-type', 'application/json')
      .send({url: ""})
      .expect(400)
  })
})

describe('GET /urls', () => {
  it('should return 200 & valid response', async () => {
    request(server)
      .get(`/api/urls`)
      .expect('Content-Type', /json/)
      .expect(200)
      .expect((res) => {
      	expect(Number(res.body.count)).toMatch(res.body.urls.length)
    	})
  })
})

describe('DELETE /delete', () => {
  it('should return 200 & valid response', async () => {
    request(server)
      .delete(`/api/delete`)
      .expect('Content-Type', /json/)
      .expect(200)
  })
})


