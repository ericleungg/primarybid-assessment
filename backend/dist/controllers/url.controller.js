"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const log_1 = __importDefault(require("../log"));
const url_model_1 = __importDefault(require("../models/url.model"));
const crypto_1 = __importDefault(require("crypto"));
// Create and Save a new Url
const create = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    log_1.default.info(req.body);
    // Validate request
    if (!req.body.url) {
        res.status(400).send({ message: "Content can not be empty!" });
        return;
    }
    log_1.default.info(req.body.url);
    let shortenedSubDir = "";
    let shortenedUrl = "";
    const longUrl = "https://pbid.io/" + req.body.url;
    let checkUrl = {};
    while (true) {
        // Create shortened subdirectory
        shortenedSubDir = crypto_1.default.randomBytes(20).toString('hex').substr(0, 8);
        shortenedUrl = "https://pbid.io/" + shortenedSubDir;
        // Check if duplicated from DB
        checkUrl = yield url_model_1.default.findOne({ url: shortenedUrl });
        if (checkUrl !== null) {
            continue;
        }
        // Create a Url
        const url = new url_model_1.default({
            shortUrl: shortenedUrl,
            longUrl
        });
        return url
            .save()
            .then((result) => {
            return res.status(201).json({
                url: result
            });
        })
            .catch((error) => {
            return res.status(500).json({
                message: error.message,
                error
            });
        });
    }
});
// Create and Save a new Url
const findAll = (req, res, next) => {
    return url_model_1.default
        .find()
        .exec()
        .then((urls) => {
        return res.status(200).json({
            urls,
            count: urls.length
        });
    })
        .catch((error) => {
        return res.status(500).json({
            message: error.message,
            error
        });
    });
};
// Create and Save a new Url
const deleteAll = (req, res, next) => {
    return url_model_1.default
        .deleteMany()
        .exec()
        .then((result) => {
        return res.status(200).json({
            result
        });
    })
        .catch((error) => {
        return res.status(500).json({
            message: error.message,
            error
        });
    });
};
exports.default = { create, findAll, deleteAll };
//# sourceMappingURL=url.controller.js.map