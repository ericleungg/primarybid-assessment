"use strict";
/* istanbul ignore file */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const models_1 = require("./models");
// import {MongoMemoryServer} from 'mongodb-memory-server'
// import config from '@exmpl/config'
const log_1 = __importDefault(require("./log"));
mongoose_1.default.Promise = global.Promise;
mongoose_1.default.set('debug', process.env.DEBUG !== undefined);
const opts = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    keepAlive: true,
    keepAliveInitialDelay: 300000,
    serverSelectionTimeoutMS: 5000,
    socketTimeoutMS: 45000 // Close sockets after 45 seconds of inactivity
};
class MongoConnection {
    // private _mongoServer?: MongoMemoryServer
    static getInstance() {
        if (!MongoConnection._instance) {
            MongoConnection._instance = new MongoConnection();
        }
        return MongoConnection._instance;
    }
    open() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                // if (config.mongo.url === 'inmemory') {
                //   logger.debug('connecting to inmemory mongo db')
                //   this._mongoServer = new MongoMemoryServer()
                //   const mongoUrl = await this._mongoServer.getConnectionString()
                //   await mongoose.connect(mongoUrl, opts)
                // } else {
                log_1.default.debug('connecting to mongo db: ' + models_1.db.dbUrl);
                mongoose_1.default.connect(models_1.db.dbUrl, opts);
                // }
                mongoose_1.default.connection.on('connected', () => {
                    log_1.default.info('Mongo: connected');
                });
                mongoose_1.default.connection.on('disconnected', () => {
                    log_1.default.error('Mongo: disconnected');
                });
                mongoose_1.default.connection.on('error', (err) => {
                    log_1.default.error(`Mongo:  ${String(err)}`);
                    if (err.name === "MongoNetworkError") {
                        setTimeout(() => {
                            mongoose_1.default.connect(models_1.db.dbUrl, opts).catch(() => { log_1.default.error(""); });
                        }, 5000);
                    }
                });
            }
            catch (err) {
                log_1.default.error(`db.open: ${err}`);
                throw err;
            }
        });
    }
    close() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield mongoose_1.default.disconnect();
                // if (config.mongo.url === 'inmemory') {
                //   await this._mongoServer!.stop()
                // }
            }
            catch (err) {
                log_1.default.error(`db.open: ${err}`);
                throw err;
            }
        });
    }
}
exports.default = MongoConnection.getInstance();
//# sourceMappingURL=db.js.map