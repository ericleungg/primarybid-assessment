"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("./index");
const log_1 = __importDefault(require("./log"));
(0, index_1.createServer)()
    .catch(err => {
    log_1.default.error(`Error: ${err}`);
});
//# sourceMappingURL=app.js.map