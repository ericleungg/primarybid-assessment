"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dbConfig = void 0;
require("../lib/env");
const { DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME, } = process.env;
const dbConfig = {
    dbUrl: (process.env.WAIT_HOSTS)
        ? "mongodb://" + process.env.WAIT_HOSTS + `/${DB_NAME}`
        : `mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`
};
exports.dbConfig = dbConfig;
//# sourceMappingURL=db.config.js.map