"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createServer = void 0;
const http_1 = __importDefault(require("http"));
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
const log_1 = __importDefault(require("./log"));
const models_1 = require("./models");
const url_routes_1 = __importDefault(require("./routes/url.routes"));
/* eslint-disable import/first */
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
function createServer() {
    return __awaiter(this, void 0, void 0, function* () {
        const router = (0, express_1.default)();
        const corsOptions = {
            origin: "http://localhost:8081"
        };
        router.use((0, cors_1.default)(corsOptions));
        // Connect to MongoDB
        yield models_1.db.mongoose
            .connect(models_1.db.dbUrl, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
            .then(() => {
            log_1.default.info("Connected to the database!");
        })
            .catch((err) => {
            log_1.default.info("Cannot connect to the database!", err);
            process.exit();
        });
        /** Parse the body of the request */
        router.use(body_parser_1.default.urlencoded({ extended: true }));
        router.use(body_parser_1.default.json());
        /** Routes go here */
        router.use('/api', url_routes_1.default);
        /** Error handling */
        router.use((req, res, next) => {
            const error = new Error('Not found');
            res.status(404).json({
                message: error.message
            });
        });
        const httpServer = http_1.default.createServer(router);
        const PORT = process.env.PORT || 8080;
        httpServer.listen(PORT, () => log_1.default.info(`Server is running on port ${PORT}.`));
        return router;
    });
}
exports.createServer = createServer;
//# sourceMappingURL=index.js.map