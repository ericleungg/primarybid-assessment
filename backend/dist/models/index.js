"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.db = void 0;
const db_config_1 = require("../config/db.config");
const mongoose_1 = __importDefault(require("mongoose"));
mongoose_1.default.Promise = global.Promise;
const db = {
    mongoose: mongoose_1.default,
    dbUrl: db_config_1.dbConfig.dbUrl,
    urls: require("./url.model")
};
exports.db = db;
//# sourceMappingURL=index.js.map