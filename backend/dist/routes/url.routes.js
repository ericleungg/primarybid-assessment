"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const express_1 = __importDefault(require("express"));
const url_controller_1 = __importDefault(require("../controllers/url.controller"));
const router = express_1.default.Router();
router.get('/urls', url_controller_1.default.findAll);
router.post('/create/url', url_controller_1.default.create);
router.delete('/delete', url_controller_1.default.deleteAll);
module.exports = router;
//# sourceMappingURL=url.routes.js.map