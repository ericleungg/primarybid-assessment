import http from "../http-common";

class UrlDataService {
  getAll() {
    return http.get("/urls");
  }

  // get(id: string) {
  //   return http.get(`/tutorials/${id}`);
  // }

  create(data: any) {
    return http.post("/create/url", data);
  }

  deleteAll() {
    return http.delete("/delete");
  }
}

export default new UrlDataService();