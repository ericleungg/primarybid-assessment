export default interface Url {
  _id: null;
  shortUrl: string;
  longUrl: string;
}