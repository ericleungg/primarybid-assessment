/**
 * @jest-environment jsdom
 */
 
// src/components/__tests__/HelloWorld.spec.ts
import { shallowMount } from '@vue/test-utils'
import UrlList from '../UrlList.vue'
import flushPromises from 'flush-promises'


describe('UrlList.vue', () => {
    // New Url Data
    const newUrl = {
        _id: "123",
        shortUrl: "https://pbid.io/1a2b3c4d",
        longUrl: "https://pbid.io/testing"
    }
    const wrapper = shallowMount(UrlList)

    it('Adding new URL', async () => {
        // Empty input
        expect(wrapper.vm.$data.newUrl.longUrl).toEqual("")

        // Set value "testing" to input
        const input = wrapper.find('input#newUrlInput')
        await input.setValue("testing")

        // newUrl.longUrl data should be updated
        expect(wrapper.vm.$data.newUrl.longUrl).toEqual("testing")

        // TODO: test (mock) api call
        wrapper.vm.$data.newUrl = newUrl
        wrapper.vm.$data.submitted = true

        await wrapper.vm.$nextTick()

        // "Add Another New Url" button should appear after submitted
        const anotherButton = wrapper.find('button#addAnotherUrlButton')
        expect(anotherButton.exists()).toBe(true)

        await anotherButton.trigger('click')

        // Get back to original "Add Url" Button
        expect(input.exists()).toBe(true)
    })
  
    it('Updating URL List and viewing original URL', async () => {
        // Update Url list
        wrapper.vm.$data.urls = {urls: [newUrl]}
        await wrapper.vm.$nextTick()

        // Should see the new url on the list
        const appendedLi = wrapper.find('li')
        expect(appendedLi.text()).toBe("https://pbid.io/1a2b3c4d")

        const pleaseClickOnUrl = wrapper.find('p#clickDetail')
        expect(pleaseClickOnUrl.exists()).toBe(true)

        // Should see the long/original url of the new url when clicking the shortened url
        await appendedLi.trigger('click')
        const originalUrl = wrapper.find('div#original_url')
        expect(originalUrl.text()).toContain("https://pbid.io/testing")
    })
})




